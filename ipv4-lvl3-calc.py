from math import ceil, log2

# Funktion zur Überprüfung der Benutzeradresse
def validate_user_address(user_address): 
    # Teile die Benutzeradresse anhand des Trennzeichens "/" auf
    parts = user_address.split('/')
    
    # Überprüfe, ob die Adresse zwei Teile hat
    if len(parts) != 2:
        return False
    
    # Trenne die Netzwerkadresse und die CIDR-Zahl
    network_address, cidr = parts
    
    # Teile die Netzwerkadresse in Oktette auf
    octets = network_address.split('.')
    
    # Überprüfe, ob die Adresse vier Oktette hat
    if len(octets) != 4:
        return False 
    try:
        # Überprüfe, ob die Oktette gültige Zahlen enthalten
        for octet in octets:
            if not 0 <= int(octet) <= 255:
                return False
        
        # Überprüfe, ob die CIDR-Zahl im gültigen Bereich liegt
        cidr_int = int(cidr)
        if not 0 <= cidr_int <= 32:
            return False
        
        return True
    except ValueError:
        return False

# Funktion zur Unterteilung des Subnetzes
def divide_subnet(network_subnet):
    # Teile das Subnetz in 8-Bit-Teile auf
    subnet_parts = [network_subnet[i:i+8] for i in range(0, 32, 8)]
    # Konvertiere die binären Teile in Dezimalzahlen und verbinde sie
    subnet_decimal = '.'.join(str(int(part, 2)) for part in subnet_parts)
    return subnet_decimal

# Funktion zur Unterteilung der Adresse
def divide_address(user_address):
    # Trenne die Netzwerkadresse und die CIDR-Zahl
    network_address, cidr = user_address.split('/')
    # Erzeuge das Subnetz basierend auf der CIDR-Zahl
    network_subnet = '1' * int(cidr) + '0' * (32 - int(cidr))
    # Konvertiere das Subnetz in Dezimalform
    subnet_decimal = divide_subnet(network_subnet)
    return subnet_decimal, network_address, cidr

# Funktion zur Berechnung der Basisadresse
def calc_base_address(network_address, subnet_decimal):
    # Zerlege die IP-Adresse und die Subnetzmaske in Oktette
    ip_octets = [int(octet) for octet in network_address.split('.')]
    mask_octets = [int(octet) for octet in subnet_decimal.split('.')]
    # Berechne die Netzwerkadresse durch logische AND-Verknüpfung
    network_octets = [ip_octets[i] & mask_octets[i] for i in range(4)]
    return '.'.join(map(str, network_octets))

# Funktion zur Berechnung der neuen CIDR-Zahl und Subnetzmaske
def calc_new_cidr_and_mask(cidr, num_subnets):
    # Berechne die neue CIDR-Zahl
    subnet_log = ceil(log2(num_subnets))
    new_cidr = int(cidr) + subnet_log
    
    # Erzeuge die neue Subnetzmaske
    new_subnet_bin = '1' * new_cidr + '0' * (32 - new_cidr)
    new_subnet_parts = [new_subnet_bin[i:i+8] for i in range(0, 32, 8)]
    new_subnet_decimal = '.'.join(str(int(part, 2)) for part in new_subnet_parts)
    
    return new_cidr, new_subnet_bin, new_subnet_decimal

# Funktion zur Berechnung der Anzahl der Hosts
def calc_hosts(new_subnet_bin):
    # Zähle die Anzahl der Host-Bits
    host_bits = new_subnet_bin.count('0')
    # Berechne die Gesamtzahl und die verwendbaren Hosts
    total_hosts = 2 ** host_bits
    usable_hosts = total_hosts - 2 
    
    return total_hosts, usable_hosts

# Funktion zur Umwandlung einer IP-Adresse in eine Ganzzahl
def calc_ip2int(ip):
    a, b, c, d = [int(i) for i in ip.split('.')]
    return (a << 24) + (b << 16) + (c << 8) + d

# Funktion zur Umwandlung einer Ganzzahl in eine IP-Adresse
def calc_int2ip(i):
    return '%d.%d.%d.%d' % (
        (i & 0b11111111000000000000000000000000) >> 24,
        (i & 0b00000000111111110000000000000000) >> 16,
        (i & 0b00000000000000001111111100000000) >> 8,
        i & 0b00000000000000000000000011111111
        )

repeat = True
while repeat:
    # Benutzereingabe der IP-Adresse
    user_address = input("Geben Sie die IP-Adresse in CIDR-Notation ein (z. B. 10.10.128.0/24): ")
    # Überprüfe die Gültigkeit der Benutzereingabe
    if not validate_user_address(user_address):
        print("Ungültige Eingabe. Bitte geben Sie eine gültige IPv4-Netzwerkadresse in CIDR-Notation ein.")
        continue
    
    # Teile die Benutzeradresse auf
    subnet_decimal, network_address, cidr = divide_address(user_address)

    # Berechne die Basisadresse
    base_address = calc_base_address(network_address, subnet_decimal)

    # Benutzereingabe für die Anzahl der Subnetze
    num_subnets = int(input("Geben Sie die Anzahl der gewünschten Subnetze ein (1-32): ")) 

    # Berechne die neue CIDR-Zahl und Subnetzmaske
    new_cidr, new_subnet_bin, new_subnet_decimal = calc_new_cidr_and_mask(cidr, num_subnets)

    # Berechne die Anzahl der Hosts
    total_hosts, usable_hosts = calc_hosts(new_subnet_bin)
    if usable_hosts < 1:
        print('Ungültige Eingabe, es ist nicht möglich, diese Hostnummer zu erhalten. Versuchen Sie es erneut.')
        continue
    
    # Berechne die Basisadresse in Ganzzahl
    ip_int = calc_ip2int(base_address)
    mask_int = calc_ip2int(new_subnet_decimal)
    # Gib die Ergebnisse aus
    print(f'Ihre Basisnetzwerk ist {base_address}/{cidr}, mit Subnetzmaske {subnet_decimal}')
    print(f'Für {num_subnets} Subnetze haben Sie {usable_hosts} verwendbare Hosts für jedes Subnetz')
    print(f'Ihre neue Subnetzmaske für jedes Subnetz ist {new_subnet_decimal}')
    for i in range(num_subnets):
        print('Basis Netz:', calc_int2ip(ip_int)+'/'+str(new_cidr), 'Erste IP:', calc_int2ip(ip_int + 1), 'Letzte IP:', calc_int2ip(ip_int | ~mask_int - 1))
        ip_end = calc_int2ip(ip_int | ~mask_int)
        ip_int = calc_ip2int(ip_end) + 1
        
    # Abfrage, ob der Benutzer eine weitere Berechnung durchführen möchte
    repeat_calculation = input("Möchten Sie eine weitere Berechnung durchführen? (Ja/Nein): ").lower()
    if repeat_calculation != "ja":
        repeat = False